﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abstract_factory
{
    public interface IAbstractFactory
    {
        IAbstractMotherboard CreateMotherboard();
        IAbstractCPU CreateCPU();
    }

    public interface IAbstractMotherboard
    {
        string FunctionMotherboard();

        string FunctionMotherboard(IAbstractCPU collaborator);
    }

    public interface IAbstractCPU
    {
        string FunctionCPU();
    }

    class Dell : IAbstractFactory
    {
        public IAbstractCPU CreateCPU()
        {
            return new CPU("Dell");
        }

        public IAbstractMotherboard CreateMotherboard()
        {
            return new Motherboard("Dell");
        }
    }

    class Sony : IAbstractFactory
    {
        public IAbstractCPU CreateCPU()
        {
            return new CPU("Sony");
        }

        public IAbstractMotherboard CreateMotherboard()
        {
            return new Motherboard("Sony");
        }
    }

    class CPU : IAbstractCPU
    {
        public string Name { get; set; }
        public CPU(string name)
        {
            Name = name;
        }
        public string FunctionCPU()
        {
            return $"{Name} CPU";
        }
    }

    class Motherboard : IAbstractMotherboard
    {
        public string Name { get; set; }
        public Motherboard(string name)
        {
            Name = name;
        }
        public string FunctionMotherboard()
        {
            return $"{Name} Motherboard";
        }

        public string FunctionMotherboard(IAbstractCPU collaborator)
        {
            var result = collaborator.FunctionCPU();
            return $"This is {Name} Motherboard with {result}";
        }
    }

    class Client
    {
        public void Main()
        {
            ClientMethod(new Dell());
            Console.WriteLine();

            ClientMethod(new Sony());
        }

        public void ClientMethod(IAbstractFactory factory)
        {
            IAbstractCPU CPU = factory.CreateCPU();
            IAbstractMotherboard motherboard = factory.CreateMotherboard();

            //Console.WriteLine(motherboard.FunctionMotherboard());
            Console.WriteLine(motherboard.FunctionMotherboard(CPU));
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            new Client().Main();
            Console.ReadLine();
        }
    }
}
