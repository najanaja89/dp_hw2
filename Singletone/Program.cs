﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singletone
{
    
    class Singleton
    {
        private static int count =0;
        private Singleton() { }
        private static Singleton _instance;
        public static Singleton GetInstance()
        {
            if(_instance==null)
            {
                _instance = new Singleton();
            }
            return _instance;
        }
        public static int myLogic()
        {
           return count++;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Singletone");
            Singleton s1 = Singleton.GetInstance();
            Singleton s2 = Singleton.GetInstance();

            Singleton.myLogic();
            Singleton.myLogic();

            if (s1 == s2)
            {
                Console.WriteLine(Singleton.myLogic().ToString());
            }
            else
            {
                Console.WriteLine("Singletone not works");
            }
            Console.ReadLine();
        }
    }
}
